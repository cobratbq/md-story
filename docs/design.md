# Design

The design based on the requirements of the story engine.

- code-blocks as the mechanism for introducing technical components/scripting into existing story?
- Go-based code-base for easy adoption?
- Go-derived scripting language for easy integration, expressiveness?
  - [Tengo](https://github.com/d5/tengo)
  - [Starlark](https://github.com/google/starlark-go)
- 

# Structure

__Story__
- `story.md` _primary concern: chronology, story-guidance_
- `"Egypt"` _primary concern: content, dedicated to specific topic_
  - images
  - videos
  - scripting
  - story-line
  - navigation
- `"Russia"`
  - ...
- `"Area 51"`
  - ...
