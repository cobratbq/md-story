# Requirements

The requirements (features) and other architectural requirements for the story engine.

## Introduction

The original idea is to compose a Markdown-based data format that supports an (interactive) story-line with extensive capabilities for iterative improvement. The Markdown document should be readable as a story written as a book, but also be able to include extensions that allow adding additional (interactive) components.

## Features

- (Textual) story-line
- Interactivity to various degrees.
- Unbiased representational options: more visual? more auditory? more communication/interaction?
- Branching model / choices
- Feature-rich replacement: for books, audio-books, visual novels, adventure games?
  - trying to capture the richness of games, the immersiveness of good story-telling, the awe, wonder and astonishment of VR, ...
  - allow choosing the media, not being bound to a single implementation decision.
- Easily adoptable, due to approachable format.
  - web-based "viewer"/"player" (if needed at all)
  - accessible to non-technical people
  - accessibility-features for handicapped uers
  - low threshold to entry/adoption
- Gradual extension/iteration: start with plain text story only. Add more components as you go.
  - (Story-)Medium-extensibility: text, audio, video, 3D, AR, VR, ... (content remains the same)
  - Content-extensibility: linear story, branching story-lines / choices, componentized story/reuse, dynamic story / adapting. (independent of medium, content changes)
- Portability/packaging
- Basics:
  - (interactive) story structure akin to book story structure (easy comprehension)
  - ability to arbitrarily choose appropriate media
- Narrative:
  - multiple stories in the same "universe"
  - multiple story-lines, alternative plot-lines
  - alternative media for pref/choice, telling the same message but with different medium (text, audio, video, ..)
  - variations of story-telling? (distinct parts/chapters, fluent transitionary narritive, ...)
  - controlled exposure to the narrative (book = everything-at-once, text adventure: bite-sized pieces, etc.)
- Media:
  - text
  - image (bitmap/vector-graphics)
  - animation
  - audio
  - video
- Visual:
  - styling
    - plain text
    - text adapted for attraction, readability, atmospheric illustration
    - mixed media presentation
    - mixed media with fluent integration, transition
  - templating/layouting
  - transitioning
- Auditory:
  - text-to-speech
- Interaction:
  - navigation (impl. detail of story)
  - actions
  - transitions between various media (in non-annoying way) to offer best of narrative experience
  - information expansion (show/hide sections/additional information)
  - variations:
    - none
    - "Press any key to continue"
    - multiple options
    - arbitrary points of interaction
    - key presses for certain (dedicated) actions
    - full motion control using keyboard/mouse
- Immersion:
  - perfect audio - video - text combination
  - AR/VR?
- Extensibility:
  - predefined standardized and custom extensions
- Scripting (limited, soundness?)
  - scoping based on document sections
  - guards/conditions
  - temporal statements, soundness of story-line, verifiability, correctness
- Support structure:
  - mapping/graphing in order to facilitate larger "worlds" (narrative locations, etc.)
  - analysis for mapping/graphing to help with world illustration/representation
- Device-support:
  - computers for full experience
  - tablets for casual story-consumption with most if-not-all capabilities
  - export basic subset to e-readers for limited but possible reading pleasure
    - able to export "interactivity" - generate epub - in such a way that navigating/arbitrary jumping is possible?
  - print a book (maybe not literally)
  
`TODO: illustrate from simplest single-document story to extensive, rich story, gradual evolution.`
  
`TODO: ...`

### Consumer (Reader/Player)

`TODO: ...`

### Producer (Writer)

`TODO: ...`

### Considerations

Not strictly requirements, but might be interesting to take into account.

- Consider if the same Markdown source format can be used for dynamically defined documents, i.e. documents with state - variables with certain values. For example, document with variables to determine content sections to render.
- Consider what level of dynamicity is necessary:
  - alternate between (complete) sections
  - alternate between paragraphs
  - in-content logic for up to single-word/single-letter granularity

## Open questions

- How to make things flexible while at the same time providing sufficient structure to guide the new user?
- Allow jumping between story-lines? Arbitrarily?
- Declarative syntax useful for anything?

## Architecture

- Ability to share/reuse content(-packages).
- Modularity
- Content structuring for maintainability, extended use.
- Ability to support multiple stories in a "common universe" setting.
- A way of compiling, packaging a product.

`TODO: ...`
