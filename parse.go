package mdstory

import (
	"fmt"

	"gitlab.com/golang-commonmark/markdown"
)

// ParseDocument parses a Markdown document and returns the document sections.
func ParseDocument(document []byte) []Section {
	md := markdown.New(markdown.HTML(false), markdown.XHTMLOutput(false))
	tokens := md.Parse(document)
	sections := make([]Section, 0)
	stack := make([]markdown.Token, 0)
	var current *Section
	for _, t := range tokens {
		fmt.Printf("Token: %#v\n", t)
		switch token := t.(type) {
		case *markdown.HeadingOpen:
			if len(sections) > 0 {
				var prev = &sections[len(sections)-1]
				current = &Section{chapter: prev.chapter, content: make([]string, 0)}
			} else {
				current = &Section{content: make([]string, 0)}
			}
			stack = append(stack, t)
		case *markdown.ParagraphOpen:
			if current == nil {
				current = &Section{chapter: "", content: make([]string, 0)}
			}
			stack = append(stack, t)
		case *markdown.HeadingClose:
			if len(stack) == 0 {
				panic("There should always be a stack entry when a closing tag is encountered.")
			}
			if current == nil {
				panic("There should always be a current section when a closing tag is encountered.")
			}
			stack = stack[0 : len(stack)-1]
		case *markdown.ParagraphClose:
			stack = stack[0 : len(stack)-1]
		case *markdown.Inline:
			if len(stack) == 0 {
				fmt.Println("WARN: Skipping content as we are not in a section.")
				continue
			}
			switch stack[len(stack)-1].(type) {
			case *markdown.HeadingOpen:
				fmt.Printf("Setting chapter: %v\n", token.Content)
				current.chapter = token.Content
			case *markdown.ParagraphOpen:
				fmt.Printf("Content: %v\n", token.Content)
				current.content = append(current.content, token.Content)
			default:
				panic("Unsupported block type.")
			}
		case *markdown.Hr:
			sections = append(sections, *current)
			current = &Section{chapter: current.chapter, content: make([]string, 0)}
		default:
			fmt.Printf("Ignoring token %v.\n", t)
		}
	}
	sections = append(sections, *current)
	fmt.Printf("Sections: %+v\n", sections)
	return sections
}

// Section contains a single section of the story.
type Section struct {
	chapter string
	content []string
}
